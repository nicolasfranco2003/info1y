/*
Scrivere una funzione ricorsiva che abbia il seguente prototipo:

char *inverti (char str[])

la funzione inverti dovrà restituire il puntatore ad una stringa contenente gli stessi caratteri di str ma in ordine inverso.

Esempio
Se str contiene “prova” la stringa restituita conterrà “avorp”.

Suggerimento: invertire “prova” equivale ad invertire “rova” e aggiungere una “p” in coda.

*/

/* bug: FUNZIONA SOLO CON NUMERO n FISSO POICHÈ NON POSSO INIZIALIZZARE IN MODO VARIABILE L'ARRAY */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *inverti(char str[]);

int main() {
    printf("%s\n", inverti("prova"));
    return 0;
}

char *inverti(char str[]) {
    int l = strlen(str);

    char *result = calloc(l + 1, sizeof(char));

    if (l>0) {
        char *inv = inverti(str + 1);
        strcpy(result, inv);
        free(inv);
        result[l - 1] = str[0];
        result[l] = '\0';	//superflua per l'uso di calloc
    }

    return result;
}
