/*
Si implementi una funzione string2list che riceve in ingresso una stringa str e restituisce una lista così
organizzata: la lista dovrà contenere un elemento per ciascun carattere distinto presente in str; inoltre, ogni elemento della lista dovrà
contenere sia il carattere associato a tale elemento, che il numero di volte in cui tale carattere compare all’interno della stringa str.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct nodo {
    char carattere;
    int ricorrenze;
    struct nodo *next;
};

typedef struct nodo *lista;

void inserisci(lista *l, char c) {
  if(*l==NULL) {
    *l = malloc(sizeof(struct nodo));
    (*l)->carattere = c;
    (*l)->ricorrenze = 1;
    (*l)->next = NULL;
  }
  else if((*l)->carattere == c)
    (*l)->ricorrenze++;
  else
    inserisci(&((*l)->next), c);
  }

lista string2list(char str[]) {
  lista l = NULL;
  int n = strlen(str);

  for(int i=0; i<n; i++) {
    inserisci(&l, str[i]);
  }

  return l;
}

void  stampa(lista l) {
  while (l!=NULL)
  {
    printf("%c %d\n",l->carattere,l->ricorrenze);
    l = l->next;
  }
  printf("END\n");
}

int main() {
  char str[] = "esempio";
  lista l1 = string2list(str);
  stampa(l1);
  return 0;
}
