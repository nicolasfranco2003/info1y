//

#include <stdio.h>

int main() {

    char a;
    int i=1;
    
    printf("Codifica ASCII dell'alfabeto (sia maiuscolo che minuscolo)\n\n");
    
    for(a = 'A' ; a<=122; a++){
        
        if(a<91 || a>96)
            
            printf("%d) Lettera = %c\nCodifica ASCII = %d\n\n",i++, a,a);
        
    }

    return 0;

}
