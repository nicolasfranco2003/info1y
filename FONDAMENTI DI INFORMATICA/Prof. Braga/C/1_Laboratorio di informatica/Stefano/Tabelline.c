#include <stdio.h>

int main (){

	int n=0;
	int k=0;
	printf("---------TABELLINE---------\n");
	printf("Inserisci un numero: ");
	scanf("%d", &n);
	printf("I divisori di %d sono:\n", n);
	k=n/2 + 1;
	
	if(n>=0)
		while(k>0){
			printf("%d\n", n/k);
			k--;
		}
	return 0;
	
	}
