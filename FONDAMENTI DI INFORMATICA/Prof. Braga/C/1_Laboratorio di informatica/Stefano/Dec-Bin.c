#include <stdio.h>

int main(){
	
	int dec=0, t=32, baseo=0;
	int bin[32];
	printf("---Da base DEC ad altre (MAX 9)---\n");
	printf("Scrivi in che base convertire il tuo numero: ");
	scanf("%d", &baseo);
	printf("Scrivi un numero in base dieci: ");
	scanf("%d", &dec);
	
	if(dec>=0){
		for(t=0; dec!=0; t++){     //quando il numero � positivo
			bin[t]=dec%baseo;
			dec=dec/baseo;
		}
		bin[t]=0;
	}	
	
	printf("Corrispondente numero in base %d = ", baseo);
	for(; t>=0; t--){
		printf("%d", bin[t]);
	}
	
	return 0;
}
