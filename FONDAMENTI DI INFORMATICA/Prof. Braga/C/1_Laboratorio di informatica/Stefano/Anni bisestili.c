#include <stdio.h>

int main()
{
	int anno = 0;
	
	printf("Inserisci un anno: ");
	scanf("%d",&anno);
	
	if(anno>1582) {
	if(anno%400==0)
		printf("E' bisestile");
	else if(anno%100==0)
		printf("Non e' bisestile");
	else if(anno%4==0)
		printf("E' bisestile");
	else
		printf("Non e' bisestile");
	}	
	else
		printf("Non era ancora stato stabilito se %d fosse un anno bisestile", anno);
	
	return 0;
}
