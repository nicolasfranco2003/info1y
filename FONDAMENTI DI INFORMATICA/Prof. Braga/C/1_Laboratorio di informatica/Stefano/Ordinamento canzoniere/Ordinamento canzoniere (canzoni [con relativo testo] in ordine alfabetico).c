#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#define MAX_BRANI 200
#define MAX_RIGA  100

typedef struct {
	char titolo[MAX_RIGA];
	int linea_inizio;
	int lunghezza_testo;
} Brano;

typedef struct {
	char filename[50];
	int num_brani;
	Brano brani[MAX_BRANI];
} Canzoniere;

int main(){
	
	Canzoniere canzoniere;
	Brano brano;
	int i=0, d=0, n=0;
	strcpy(canzoniere.filename, "canzoniere.txt");
	canzoniere.num_brani=189;
	FILE *file;
	FILE *file2;
	char str[MAX_RIGA]="";
	if((file=fopen("canzoniere.txt", "r"))!=NULL){
		while(!feof(file)){
			fgets(str, MAX_RIGA, file);
			d++;
			if(strcmp(str, "**********\n")==0){
				if(d!=1){
					brano.lunghezza_testo=d-brano.linea_inizio-1;
					canzoniere.brani[i]=brano;
					i++;
				}
				fgets(str, MAX_RIGA, file);
				d++;
				str[strlen(str)-1]='\0';
				strcpy(brano.titolo, str);
				brano.linea_inizio=d+2;
			}
		}
		brano.lunghezza_testo=d-brano.linea_inizio-1;
		canzoniere.brani[i]=brano;
		n=i;
		i=0;
		for(i=0; i<=n; i++){
			printf("%3d) Titolo: %39s---Linea di Inizio: %4d---Lunghezza in Righe: %2d\n", i+1, canzoniere.brani[i].titolo, canzoniere.brani[i].linea_inizio, canzoniere.brani[i].lunghezza_testo);
		}
		rewind(file);
		////////
		char str1[MAX_RIGA]="";
		char str2[MAX_RIGA]="";
		char real[MAX_RIGA]="";
		int m;
		file2=fopen("canzoniere2.txt", "w");
		for(; strcmp(real, "zzz")!=0; ){
			rewind(file);
			strcpy(real, "zzz");
			for(i=0; i<n; i++){
				strcpy(str1, canzoniere.brani[i].titolo);
				i++;
				if(i<n){
					strcpy(str2, canzoniere.brani[i].titolo);
				}
				if(strcmp(str1, str2)==-1 && strcmp(real, str1)==1){
					strcpy(real, str1);
				}
				else if(strcmp(str1, str2)==1 && strcmp(real, str2)==1){
					strcpy(real, str2);
				}
			}
			if(strcmp(real, "zzz")!=0){
				for(i=0; strcmp(real, canzoniere.brani[i].titolo)!=0; i++){
				}
				fprintf(file2, "**********\n%s\n\n", canzoniere.brani[i].titolo);
				for(m=0; m<canzoniere.brani[i].linea_inizio; m++){
					fgets(str1, MAX_RIGA, file);
				}
				for(; m<=canzoniere.brani[i].linea_inizio+canzoniere.brani[i].lunghezza_testo-2; m++){
					fgets(str1, MAX_RIGA, file);
					fputs(str1, file2);
				}
				fputs("\n", file2);
				strcpy(canzoniere.brani[i].titolo, "zzz");
			}
		}
		///////
		fclose(file2);
		fclose(file);
	}
	else{
		printf("Errore nell'apertura del file \"canzoniere.txt\"");
	}
	return 0;
}
