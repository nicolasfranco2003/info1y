#include <stdio.h>

#define N 5

int main(){
	
	int matrix[N][N];
	printf("--La matrice impazzita--\n");
	printf("Matrice %dx%d\n", N, N);
	
	int n=1;
	int i=0;
	int j=0;
	for(i=0; i<N; i++){		//assegnamento valori alla matrice N*N
		j=0;
		matrix[i][j]=n;
		++n;
		for(j=1; j<N; j++){
			matrix[i][j]=n;
			++n;
		}
	}
	
	i=0;		//stampa della matrice quadrata
	j=0;
	for(i=0; i<N; i++){
		j=0;
		printf("\n%4d,", matrix[i][j]);
		for(j=1; j<N; j++){
			printf("%4d,", matrix[i][j]);
		}
	}
	
	printf("\n\nInserire riga e colonna della cella: ");
	int imax=0, jmax=0;
	scanf("%d%d", &imax, &jmax);
	
	printf("\n");
	printf("Numero selezionato: %d\n\n", matrix[imax][jmax]);
	
	i=0;
	j=0;
	for(i=0; i<=imax; i++){		//modifica matrici PRECEDENTI
		j=0;
		matrix[i][j]+=matrix[imax][jmax];
		for(j=j+1; j<N && (i<imax || j<jmax); j++){
			if(j==jmax){
				matrix[i][j]=((matrix[imax][jmax])*2);
			}
			else{
				matrix[i][j]+=matrix[imax][jmax];
			}
		}
	}
	
	i=imax;
	j=jmax+1;
	
	for(; i<N && (i>imax || j>jmax); i++){
		matrix[i][j]-=matrix[imax][jmax];
		j++;
		for(; j<N && (i>imax || j>jmax); j++){
			if(j==jmax){
				matrix[i][j]=(matrix[imax][jmax])*2;
			}
			else{
				matrix[i][j]-=matrix[imax][jmax];
			}
		}
		j=0;
	}
	
	printf("\n");
	
	i=0;		//stampa della NUOVA matrice
	j=0;
	for(i=0; i<N; i++){
		j=0;
		printf("\n%4d,", matrix[i][j]);
		for(j=1; j<N; j++){
			printf("%4d,", matrix[i][j]);
		}
	}
	
	return 0;
}
