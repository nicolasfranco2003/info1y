#include <stdio.h>

#define MAX 5

int div (int *n, int dc){

	//printf("num fun: %d\n", *n);

	if(*n%dc != 0)

		return (div(n,dc+1));

	*n = *n/dc;

	return dc; //returna il divisore minimo

}

int min_div(int *num){

	if(*num<=0)

		printf(" FUN MIN_DIV Negativo!\n");

	if (*num == 1)

		return 1;

	return div(num,2); //returna il risultato della divisione

}

void sfp(int n){

	int md;

	md = min_div(&n);

	if(md>1)

		printf(" FUN SFP: %d\n", md );

	if(n>1)

		sfp(n);

	return ;

}

int main(void){

	int vect [MAX]={13,15,17,122,360};

	printf("Inizio funzione div: %d\n", div(vect,2));

	printf("Inizio funzione min_div: %d\n", min_div(vect));

	for(int i=1; i<MAX; i++){

		printf("ciclo for: %d\n",vect[i]); 
		sfp(vect[i]);
		printf("\n");

	}

}